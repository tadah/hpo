#ifndef TADAH_HPO_H
#define TADAH_HPO_H

#include <tadah/core/config.h>

int hpo_run(Config&, std::string &trg_file,
    std::string &val_file);

#endif
